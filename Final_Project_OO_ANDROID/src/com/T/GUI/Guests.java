package com.T.GUI;

import java.lang.ref.ReferenceQueue;
import java.sql.Connection;
import java.util.ArrayList;

import com.T.test.Controll;
import com.T.test.Film;
import com.T.test.Guest;
import com.T.test.Manager;
import com.T.test.R;
import com.T.test.Referee;
import com.T.test.User;
import com.T.test.R.id;
import com.T.test.R.layout;
import com.T.test.R.menu;
import com.T.test.R.string;

import DB.DB;
import DB.FilmRepository;
import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Guests extends Activity implements OnCheckedChangeListener,
		OnItemLongClickListener, OnItemClickListener {

	private Controll control;
	ListView list;
	private ArrayAdapter<String> adapter;
	private Manager g;
	private EditText textview;
	private CheckBox chkbox;
	private Spinner spinner;
	private String username;
	private String password;
	private Menu menu;
	Referee ref;

	

	ArrayList<Film> s = new ArrayList<Film>();;
	int readclickid = -1;
	String realnameclicked = "";
	private int currentstate;

	final int USER = 1;
	final int GUEST = 2;
	final int REFEREE = 3;
	final int MANAGER = 4;
	private Context content;
	private int positions;

	LinearLayout linearlayout22;
	private Spinner spinner2;
	private LinearLayout linearlayout11;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.guestlayout);
		list = (ListView) findViewById(R.id.listView1);
		textview = (EditText) findViewById(R.id.edt);
		chkbox = (CheckBox) findViewById(R.id.checkBox1);
		chkbox.setOnCheckedChangeListener(this);
		spinner = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		linearlayout22 = (LinearLayout) findViewById(R.id.layout22);
		linearlayout11 = (LinearLayout) findViewById(R.id.layout);
		adapter = new ArrayAdapter<String>(getApplicationContext(),
				R.layout.text);

		list.setAdapter(adapter);
		// control = new Controll();
		// control.converJsonToText();

		initialstate();
	}

	private void initialstate() {

		Intent bundle = getIntent();
		username = bundle.getExtras().getString("username");
		password = bundle.getExtras().getString("password");
		currentstate = bundle.getExtras().getInt("currentstate");

		if (currentstate == USER) {
			((Button)findViewById(R.id.button1)).setText(getResources().getString(R.string.search));
			
			g = new User();
			String[] item = { "Name", "Year" ,"Duration" , "Director" ,"Country"};
			spinner.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, item));

			spinner2.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, item));

			g.initailState();

		} else if (currentstate == GUEST) {
			((Button)findViewById(R.id.button1)).setText(getResources().getString(R.string.search));
			linearlayout22.setVisibility(View.GONE);
			g = new Guest();
			chkbox.setVisibility(View.GONE);
			String[] item = { "Name" };
			spinner.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, item));
			g.initailState();
		} else if (currentstate == REFEREE) {
			((Button)findViewById(R.id.button1)).setText(getResources().getString(R.string.seecommenrreferree));
			linearlayout11.setVisibility(View.GONE);
			linearlayout22.setVisibility(View.GONE);
			ref = new Referee();
			chkbox.setVisibility(View.GONE);
			spinner.setVisibility(View.GONE);
			ref.initailState();
		} else if (currentstate == MANAGER) {
			((Button)findViewById(R.id.button1)).setText(getResources().getString(R.string.search));
			
			g = new Manager();
			String[] item = { "Name", "Year" ,"Duration" , "Director" ,"Country"};
			spinner.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, item));
			spinner2.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, item));
			g.initailState();
		}

		if (currentstate == USER || currentstate == MANAGER)
			list.setOnItemLongClickListener(this);
		else if (currentstate == REFEREE) {
			textview.setVisibility(View.GONE);
			list.setOnItemClickListener(this);
		}
	}

	public void gustbtnsearch(View v) {

		synchronized (this) {

			new astntask().execute();
		}
		// adapter.add(control.isavailabledata("La Strada"));
	}

	@SuppressLint("NewApi")
	private class astntask extends AsyncTask<Void, Void, ArrayList<Film>> {
		private ArrayList<String> Reffreereult;

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			adapter.clear();
			super.onPreExecute();
		}

		@Override
		protected ArrayList<Film> doInBackground(Void... params) {
			if (currentstate == USER || currentstate == GUEST
					|| currentstate == MANAGER) {
				Log.d("ADASD", s + "   " + adapter + "  " + g + "  "
						+ spinner.getSelectedItem().toString() + "  "
						+ textview.getText().toString());
				s = g.Search(textview.getText().toString(), adapter, spinner
						.getSelectedItem().toString());
			} else if (currentstate == REFEREE) {
				Reffreereult = ref.checkcomments(username);

			}

			return s;
		}

		@Override
		protected void onPostExecute(ArrayList<Film> result) {
			if (currentstate == USER || currentstate == MANAGER) {
				g.initialSearch(result, chkbox.isChecked(), spinner2
						.getSelectedItem().toString());
				if (s.size() == 0) {
					Toast.makeText(getApplicationContext(),
							"There are not any result", Toast.LENGTH_LONG)
							.show();
				} else
				for (Film ss : result) {
					adapter.add(ss.toString());
				}
				
			}else if(currentstate == GUEST){
				if (s.size() == 0) {
					Toast.makeText(getApplicationContext(),
							"There are not any result", Toast.LENGTH_LONG)
							.show();
				} else
				for (Film ss : result) {
					adapter.add(ss.toString());
				}
			}
			
			
			else if (currentstate == REFEREE) {
				if (Reffreereult.size() == 0) {
					Toast.makeText(getApplicationContext(),
							"There are not any result", Toast.LENGTH_LONG)
							.show();
				} else
					for (String ss : Reffreereult) {
						adapter.add(ss);
					}
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked)
			chkbox.setText(getResources().getString(R.string.desort));
		else
			chkbox.setText(getResources().getString(R.string.sort));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (currentstate == USER) {
			this.menu = menu;
			getMenuInflater().inflate(R.menu.user, menu);
		} else if (currentstate == MANAGER) {
			this.menu = menu;
			getMenuInflater().inflate(R.menu.managernoclick, menu);
		} else if (currentstate == GUEST) {
			this.menu = menu;
			getMenuInflater().inflate(R.menu.guset, menu);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.changeuserandpassword) {

			final Dialog dialog = new Dialog(this);
			dialog.setTitle("Changing username and password");
			dialog.setContentView(R.layout.usandpaass);
			dialog.findViewById(R.id.btndialog).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks().execute(dialog);
						}

					});
			dialog.show();

			return true;

		} else if (id == R.id.commenent) {
			Intent i = new Intent(this, commentActivity.class);
			i.putExtra("readclickid", readclickid);
			i.putExtra("realnameclicked", realnameclicked);
			startActivity(i);

		} else if (id == R.id.newfilm) {
			final Dialog dialog11 = new Dialog(this);
			dialog11.setContentView(R.layout.changenewinputfilm);

			dialog11.findViewById(R.id.btnchangeornewdata).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks().execute(dialog11);
						}

					});
			dialog11.show();
		} else if (id == R.id.edit) {

			final Dialog dialog111 = new Dialog(this);

			dialog111.setContentView(R.layout.changenewinputfilm);
			dialog111.setTitle("Edit Data");
			((EditText) (dialog111.findViewById(R.id.name))).setHint(s.get(
					positions).getName());
			((EditText) (dialog111.findViewById(R.id.year))).setHint(s.get(
					positions).getYear()
					+ "");

			((EditText) (dialog111.findViewById(R.id.country))).setHint(s.get(
					positions).getCountry());
			((EditText) (dialog111.findViewById(R.id.duration))).setHint(s.get(
					positions).getDurationMinutes()
					+ "");
			((EditText) (dialog111.findViewById(R.id.director))).setHint(s.get(
					positions).getDirector());
			((EditText) (dialog111.findViewById(R.id.describ))).setHint(s.get(
					positions).getDescription());
			String genrelist = "";
			int i = 0;
			for (String sss : s.get(positions).getGenreList()) {
				if (s.size() == i)
					genrelist += sss + ",";
				else
					genrelist += sss;
				i++;
			}
			((EditText) (dialog111.findViewById(R.id.genrelist)))
					.setHint(genrelist);

			dialog111.findViewById(R.id.btnchangeornewdata).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks1111().execute(dialog111);
						}

					});
			dialog111.show();

		} else if (id == R.id.date) {
			Intent i = new Intent(this, resultbetweendate.class);
			startActivity(i);
		} else if (id == R.id.newuser) {
			final Dialog dialog = new Dialog(this);
			dialog.setTitle("Create new user");
			dialog.setContentView(R.layout.usandpaass);
			dialog.findViewById(R.id.btndialog).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks().execute(dialog);
						}

					});
			dialog.show();
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressLint("NewApi")
	private class astntasks extends AsyncTask<Dialog, Void, Void> {
		private String newusername;
		private String newpassword;

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Dialog... dialog) {
			if (currentstate == USER) {
				newusername = ((EditText) (dialog[0]
						.findViewById(R.id.username))).getText().toString();
				newpassword = ((EditText) (dialog[0].findViewById(R.id.pasword)))
						.getText().toString();
				g.changepassword(username, password, newusername, newpassword);
				username = newusername;
				password = newpassword;
			} else if (currentstate == MANAGER) {
				String name = ((EditText) (dialog[0].findViewById(R.id.name)))
						.getText().toString();
				int year = Integer.parseInt(((EditText) (dialog[0]
						.findViewById(R.id.year))).getText().toString());
				String country = ((EditText) (dialog[0]
						.findViewById(R.id.country))).getText().toString();
				int durationMinutes = Integer.parseInt(((EditText) (dialog[0]
						.findViewById(R.id.year))).getText().toString());
				String director = ((EditText) (dialog[0]
						.findViewById(R.id.director))).getText().toString();
				String description = ((EditText) (dialog[0]
						.findViewById(R.id.describ))).getText().toString();
				String[] genrelist = ((EditText) (dialog[0]
						.findViewById(R.id.genrelist))).getText().toString()
						.split(",");

				g.inputnewfilm(name, year, country, durationMinutes, director,
						description, genrelist);
			} else if (currentstate == GUEST) {
				newusername = ((EditText) (dialog[0]
						.findViewById(R.id.username))).getText().toString();
				newpassword = ((EditText) (dialog[0].findViewById(R.id.pasword)))
						.getText().toString();
				g.newUserCreate(newusername, newpassword);
			}
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			if (currentstate == USER) {
				Toast.makeText(getApplicationContext(),
						"Change password successfully", Toast.LENGTH_LONG)
						.show();
			} else if (currentstate == MANAGER) {
				Toast.makeText(getApplicationContext(), "Add new film",
						Toast.LENGTH_LONG).show();
			} else if (currentstate == GUEST) {
				Toast.makeText(getApplicationContext(), "Add new user",
						Toast.LENGTH_LONG).show();
			}
		}

	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
			int position, long arg3) {
		
		if (currentstate == USER) {
			Log.d("ASDASD", "DASD");
			menu.clear();
			getMenuInflater().inflate(R.menu.remove_comment, menu);
			positions = position;
			readclickid = s.get(position).getId();
			realnameclicked = s.get(position).getName();
		} else if (currentstate == MANAGER) {
			positions = position;
			menu.clear();
			getMenuInflater().inflate(R.menu.managercliked, menu);
			readclickid = s.get(position).getId();
			realnameclicked = s.get(position).getName();

		}

		return true;

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
			long arg3) {

		if (currentstate == REFEREE) {
			positions = position;
			new AlertDialog.Builder(this)
					.setTitle(getResources().getString(R.string.error))
					.setMessage(getResources().getString(R.string.accept))
					.setPositiveButton(
							getResources().getString(R.string.Vote_OK),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									new astntasks6().execute(position, 1);
								}
							})
					.setNegativeButton(
							getResources().getString(R.string.Vote_cancel),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									new astntasks6().execute(position, 2);

								}
							})

					.show();
		}

	}

	@SuppressLint("NewApi")
	private class astntasks6 extends AsyncTask<Integer, Void, Void> {

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Integer... position) {
			if (currentstate == REFEREE) {
				Log.d("ASDASD", position[0] +"  "+position[1]);
				ref.okComment(username, list.getChildAt(position[0])
						.toString(), position[1]);
			}

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
           adapter.remove(adapter.getItem(positions));
			Toast.makeText(getApplicationContext(), "Set data",
					Toast.LENGTH_LONG).show();

		}

	}

	@SuppressLint("NewApi")
	private class astntasks1111 extends AsyncTask<Dialog, Void, Void> {
		private String newusername;
		private String newpassword;

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Dialog... dialog) {
			Log.d("ASDASD", s.get(0).getName());
			if (currentstate == MANAGER) {
				String name = ((EditText) (dialog[0].findViewById(R.id.name)))
						.getText().toString();

				int year = Integer.parseInt(((EditText) (dialog[0]
						.findViewById(R.id.year))).getText().toString());

				String country = ((EditText) (dialog[0]
						.findViewById(R.id.country))).getText().toString();

				int durationMinutes = Integer.parseInt(((EditText) (dialog[0]
						.findViewById(R.id.duration))).getText().toString());

				String director = ((EditText) (dialog[0]
						.findViewById(R.id.director))).getText().toString();

				String description = ((EditText) (dialog[0]
						.findViewById(R.id.describ))).getText().toString();

				String[] genrelist = ((EditText) (dialog[0]
						.findViewById(R.id.genrelist))).getText().toString()
						.split(",");

				g.updatadata(s.get(positions).getId(), s.get(positions)
						.getName(), s.get(positions).getYear(), s
						.get(positions).getCountry(), s.get(positions)
						.getDurationMinutes(), s.get(positions).getDirector(),
						s.get(positions).getGenreList(), s.get(positions)
								.getDescription(), name, year, country,
						durationMinutes, director, genrelist, description);
			}
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
           
			Toast.makeText(getApplicationContext(), "set data",
					Toast.LENGTH_LONG).show();

		}

	}
}