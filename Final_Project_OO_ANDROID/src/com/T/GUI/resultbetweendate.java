package com.T.GUI;

import java.util.ArrayList;

import com.T.test.Film;
import com.T.test.Manager;
import com.T.test.R;
import com.T.test.R.id;
import com.T.test.R.layout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class resultbetweendate extends Activity {
	Manager m;
	EditText edit1;
	EditText edit2;
	private ListView list;
	private ArrayAdapter<String> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resultbetweendate);
		m = new Manager();
		m.initailState();
		edit1 = (EditText) findViewById(R.id.edittx1);
		edit2 = (EditText) findViewById(R.id.edittx2);
		list = (ListView) findViewById(R.id.listdate);
		adapter = new ArrayAdapter<String>(getApplicationContext(),
			R.layout.text);

		list.setAdapter(adapter);
	}

	public void clickdate(View v) {
		new astntasks1111().execute();
	}

	@SuppressLint("NewApi")
	private class astntasks1111 extends
			AsyncTask<Dialog, Void, ArrayList<Film>> {

		ArrayList<Film> s;

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected ArrayList<Film> doInBackground(Dialog... dialog) {

			s = m.getDataEventBetweenTimes(edit1.getText().toString(), edit2
					.getText().toString());
			return s;

		}

		@Override
		protected void onPostExecute(ArrayList<Film> result) {

			for (Film d : result) {
				adapter.add(d.toString() + "   " + d.getDate());
			}

		}

	}
}
