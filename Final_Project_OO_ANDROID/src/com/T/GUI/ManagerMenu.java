package com.T.GUI;

import com.T.test.Manager;
import com.T.test.R;
import com.T.test.R.id;
import com.T.test.R.layout;
import com.T.test.R.string;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.StaticLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ManagerMenu extends Activity {
	Manager c;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
      
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manager);
		initialstate();
	}

	private void initialstate() {
		c = new Manager();
		c.initailState();
	}

	public void managebutton(View v) {
		switch (v.getId()) {
		case R.id.btnmng1:
			new AlertDialog.Builder(this)
					.setTitle(getResources().getString(R.string.error))
					.setMessage(getResources().getString(R.string.accually))
					.setPositiveButton(
							getResources().getString(R.string.Vote_OK),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									new astntasks6().execute(1);
								}
							})
					.setNegativeButton(
							getResources().getString(R.string.Vote_cancel),
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {

								}
							})

					.show();
			break;
		case R.id.btnmng2:

			final Dialog dialog11 = new Dialog(this);
			
			dialog11.setTitle("New referee");
			dialog11.setContentView(R.layout.usandpaass);
			((Button)dialog11.findViewById(R.id.btndialog)).setText("اعمال");
			dialog11.findViewById(R.id.btndialog).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks().execute(dialog11);
						}

					});
			dialog11.show();

			break;
			
		case R.id.btnmng3:
			Intent o = getIntent();
			
			Intent intent = new Intent(this, Guests.class);
			intent.putExtra("currentstate", o.getExtras().getInt("currentstate"));
			
			startActivity(intent);	
			break;

		}
	}

	@SuppressLint("NewApi")
	private class astntasks6 extends AsyncTask<Integer, Void, Void> {

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Integer... position) {
			if (position[0] == 1) {
				c.isertJsonToDB();
			} else if (position[0] == 2) {
				
			}

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {

			Toast.makeText(getApplicationContext(), "Change password",
					Toast.LENGTH_LONG).show();

		}

	}

	@SuppressLint("NewApi")
	private class astntasks extends AsyncTask<Dialog, Void, Void> {
		String username;
		String password;

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Dialog... dialog) {
			username = ((EditText) (dialog[0].findViewById(R.id.username)))
					.getText().toString();
			password = ((EditText) (dialog[0].findViewById(R.id.pasword)))
					.getText().toString();
			c.inputnewRefree(username, password);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
		Toast.makeText(getApplicationContext(), "insert new referee", Toast.LENGTH_LONG).show();
		}
	}

}
