package com.T.GUI;

import java.util.ArrayList;

import com.T.test.R;
import com.T.test.User;
import com.T.test.R.id;
import com.T.test.R.layout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class commentActivity extends Activity {
	User g = new User();
	int readclickid;
	private String realnameclicked;
	ListView list;
	private ArrayAdapter<String> adapter;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment);
		initial();
	}

	private void initial() {
		list = (ListView)findViewById(R.id.listcomment);
		g.initailState();
		adapter = new ArrayAdapter<String>(getApplicationContext(),
				R.layout.text);

		list.setAdapter(adapter);
		
		Intent i = getIntent();
		readclickid = i.getExtras().getInt("readclickid");
		realnameclicked = i.getExtras().getString("realnameclicked");
		Log.d("ADASD", ""+readclickid+ "  "+realnameclicked);
		new astntasks2().execute();
	}

	@SuppressLint("NewApi")
	private class astntasks2 extends AsyncTask<Dialog, Void, Void> {

		ArrayList<String> arraycomment;

		@Override
		protected void onPreExecute() {
			adapter.clear();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Dialog... dialog) {
			Log.d("ADASDss", ""+readclickid+ "  "+realnameclicked);
			arraycomment = g.showAllComment(realnameclicked, readclickid);
			Log.d("ADrwqaSD", arraycomment.size()+" ");
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			int i = 1;
			for (String a : arraycomment) {
				adapter.add("Comment"+i+":"+a);
				i++;
			}
			
		

		}

	}

	public void sendcomment(View v){
	
	new astntasks3().execute();

	
	
}
	
	

	@SuppressLint("NewApi")
	private class astntasks3 extends AsyncTask<Dialog, Void, Void> {
	

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Dialog... dialog) {
			String comment = ((EditText) (findViewById(R.id.commenttxt)))
					.getText().toString();
		    g.addComment(comment, readclickid, realnameclicked);
			return null;

		}

		@Override
		protected void onPostExecute(Void result) {

			Toast.makeText(getApplicationContext(), "Send Comment",
					Toast.LENGTH_LONG).show();

		}

	}
	
	
	
	
	
	
	
}
