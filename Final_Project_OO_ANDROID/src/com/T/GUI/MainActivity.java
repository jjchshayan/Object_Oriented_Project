package com.T.GUI;

import java.util.ArrayList;

import org.apache.log4j.PropertyConfigurator;

import com.T.test.Controll;
import com.T.test.R;
import com.T.test.R.id;
import com.T.test.R.layout;

import DB.DB;
import DB.FilmRepository;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	final int USER = 1;
	final int GUEST = 2;
	final int REFEREE = 3;
	final int MANAGER = 4;
	int currentState = -1;
	Controll c;
	public static Context cntx;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		c = new Controll();
		cntx = getApplicationContext();
	}

	public void btnclick(View v) {
		switch (v.getId()) {
		case R.id.user:
			currentState = USER;
			final Dialog dialog1 = new Dialog(this);
			dialog1.setTitle("Login");
			dialog1.setContentView(R.layout.usandpaass);
			dialog1.findViewById(R.id.btndialog).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks().execute(dialog1);
						}

					});
			dialog1.show();

			break;
		case R.id.guest:
			currentState = GUEST;
			Intent intent = new Intent(MainActivity.this, Guests.class);
			intent.putExtra("currentstate", currentState);
			startActivity(intent);
			break;
		case R.id.referee:
			currentState = REFEREE;
			final Dialog dialog11 = new Dialog(this);
			dialog11.setTitle("Login");
			dialog11.setContentView(R.layout.usandpaass);
			dialog11.findViewById(R.id.btndialog).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							new astntasks().execute(dialog11);
						}

					});
			dialog11.show();

			break;

		case R.id.admin:
			currentState = MANAGER;
			Intent intent1 = new Intent(MainActivity.this, ManagerMenu.class);
			intent1.putExtra("currentstate", currentState);
			startActivity(intent1);
			break;

		}
	}

	@SuppressLint("NewApi")
	private class astntasks extends AsyncTask<Dialog, Void, Boolean> {
		String username;
		String password;

		@Override
		protected void onPreExecute() {
			Log.d("ASDASD", "ZXCZ");
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(Dialog... dialog) {
			username = ((EditText) (dialog[0].findViewById(R.id.username)))
					.getText().toString();
			password = ((EditText) (dialog[0].findViewById(R.id.pasword)))
					.getText().toString();
			String tbname = null;
			if (currentState == USER)
				tbname = "users";
			else if (currentState == REFEREE)
				tbname = "referee";
			if (c.checkuserandpass(username, password, tbname) == true) {
				return true;
			} else
				return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			Log.d("ASDASD", "kjk;kjkj"+result);
			if (result == true) {
				Intent intent = new Intent(MainActivity.this, Guests.class);
				intent.putExtra("username", username);
				intent.putExtra("password", password);
				intent.putExtra("currentstate", currentState);
				startActivity(intent);
			} else {
				Toast.makeText(getApplicationContext(), "invalid input",
						Toast.LENGTH_LONG).show();
			}
		}
	}

}