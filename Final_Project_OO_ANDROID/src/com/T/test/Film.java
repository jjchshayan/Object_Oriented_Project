package com.T.test;

import java.util.Arrays;

public class Film {
	private int id;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	private String name;
	private int year;

	private String country;
	private String[] genreList;
	private int durationMinutes;
	private String director;
	private String description;
	private String date;

	
	public Film(String name, int year, String country, int durationMinutes,
			String director, String description, String[] genreList , String date) {

		super();
		this.genreList = genreList;
		this.name = name;
		this.year = year;
		this.country = country;
		this.durationMinutes = durationMinutes;
		this.director = director;
		this.description = description;
		this.setDate(date);
	}
	
	
	
	
	public Film(String name, int year, String country, int durationMinutes,
			String director, String description, String[] genreList) {

		super();
		this.genreList = genreList;
		this.name = name;
		this.year = year;
		this.country = country;
		this.durationMinutes = durationMinutes;
		this.director = director;
		this.description = description;
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < genreList.length; i++)
			if (i == 0)
				s = genreList[i];
			else
				s = s + "  , " + genreList[i];

		return "Name: " + name + " \n Year: " + year + "\n  Country  "
				+ country + "\n    genreList:(" + s
				+ "  )  \n      durationMinutes: " + durationMinutes
				+ "\n       director: " + director + "\n        description: "
				+ description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setGenreList(String[] genreList) {
		this.genreList = genreList;
	}

	public void setDurationMinutes(int durationMinutes) {
		this.durationMinutes = durationMinutes;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public int getYear() {
		return year;
	}

	public String getCountry() {
		return country;
	}

	public String[] getGenreList() {
		return genreList;
	}

	public int getDurationMinutes() {
		return durationMinutes;
	}

	public String getDirector() {
		return director;
	}

	public String getDescription() {
		return description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
