package com.T.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DB.FilmRepository;
import android.annotation.SuppressLint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

@SuppressLint("NewApi")
public class Controll {

	private ArrayList<Film> films;
	FilmRepository fr;

	public Controll() {
		fr = FilmRepository.getRepository();
	}

	public void converJsonToText() {
		fr = FilmRepository.getRepository();
		films = new ArrayList<Film>();
		File file = new File("movies.json");
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			Gson gson = new GsonBuilder().create();
			String i;
			while ((i = reader.readLine()) != null) {
				i = reader.readLine();

				try {
					fr.addFilm(gson.fromJson(i, Film.class));
				} catch (JsonSyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<Film> geteventbettweendates(String startdate, String enddate) {
		return fr.geteventbettweendates(startdate, enddate);
	}

	public void insetNewRefree(String name, String password) {
		fr.insetNewRefree(name, password);

	}

	public void addFilm(Film film) {
		try {
			fr.addFilm(film);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void changeNewUserdata(String usernameloggin, String passwordlogin,
			String newusername, String newupassword) {
		fr.changeNewUserdata(usernameloggin, passwordlogin, newusername,
				newupassword);

	}

	public void addComment(String comment, int id, String name) {
		fr.addComment(comment, id, name);

	}

	public ArrayList<String> searchcomment(String name, int id) {
		ArrayList<String> s = fr.searchcomment(name, id);
		return s;

	}

	public boolean checkuserandpass(String username, String password,
			String tbname) {

		return fr.checkUserAndPasswordTrue(username, password, tbname);
	}

	public void updatefilm(int id , String oldname, int oldyear, String oldcountry,
			int oldduration, String olddirector, String[] oldgenre,
			String olddescrib  ,String name, int year, String country,
			int duration, String director, String[] genre,
			String describ ) {
		fr.updatedata(id,oldname, oldyear, oldcountry, oldduration, olddirector,
				oldgenre, olddescrib, name, year, country, duration, director,
				genre, describ);

	}

}
