package com.T.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import android.util.Log;
import DB.DB;
import DB.FilmRepository;

public class User extends Manager {
	Scanner in;
	private String usernameloggin;
	private String passwordlogin;
	public static Logger log = Logger.getLogger(Manager.class);

	public void initailState() {

		control = new Controll();

	}



	private void checkPaswword() {
		System.out.println("Login User: ");
		in = new Scanner(System.in);
		System.out.println("Please insert Username and Password");
		System.out.print("UserName: ");
		String username = in.next();
		System.out.print("Password: ");
		String password = in.next();

		if (FilmRepository.getRepository().checkUserAndPasswordTrue(username,
				password, password) == true) {
			log.debug("Connected " + username + " user to databases ");
			usernameloggin = username;
			passwordlogin = password;
			System.out.println("You Are Loggin");
			System.out.println("1)Search?");
			System.out.println("2)Change username and password ?");

			in = new Scanner(System.in);
			int num = in.nextInt();

			if (num == 1) {
				Search(null, null, null);
				initialSearch(filmsearch,false,null);
				insertComment();
			} else if (num == 2) {
				in = new Scanner(System.in);
				System.out.print("User name:");
				String newusername = in.nextLine();
				System.out.print("Password: ");
				String newupassword = in.nextLine();

			} else {
				System.out.println("Error");
				System.exit(0);
			}

		} else {
			System.out.println("You Should create Account");
			System.exit(0);
		}

	}

	public void changepassword(String usernameloggin, String passwordlogin,
			String newusername, String newupassword) {

		control.changeNewUserdata(usernameloggin, passwordlogin, newusername,
				newupassword);

	}

	public void addComment(String comment , int id , String name ) {
		control.addComment(comment,id,name);
	}
	
	public ArrayList<String> showAllComment(String name , int id ) {
		Log.d("ADASD", ""+name+ "  "+id);
		ArrayList<String> s=  control.searchcomment(name, id);
	   return	s;
	}

	private void insertComment() {
		System.out.println("1) Insert comment");
		System.out.println("2) Search comment");
		System.out.println("3) Exit");

		in = new Scanner(System.in);
		int numbers = in.nextInt();
		if (numbers == 1) {
			System.out.println("Select one of the result:");
			int filmresult = in.nextInt();
			filmresult--;
			if (filmresult <= filmsearch.size()) {
				in = new Scanner(System.in);
				System.out.print("Comment:  ");
				String comment = in.nextLine();
				System.out.print(filmsearch.get(filmresult).getId() + "  "
						+ comment);

				control.addComment(comment, filmsearch.get(filmresult).getId(),
						filmsearch.get(filmresult).getName());

				log.debug("insert comment" + usernameloggin + "in "
						+ filmsearch.get(filmresult).getName() + " film");
			}
		} else if (numbers == 2) {
			in = new Scanner(System.in);
			System.out.println("Select one of the result:");
			int filmresult = in.nextInt();
			filmresult--;

			System.out.println("input name :");
			in = new Scanner(System.in);
			String name = in.nextLine();
			control.searchcomment(name, filmsearch.get(filmresult).getId());
		} else if (numbers == 3) {
			System.exit(0);
		} else {
			System.out.println("Error");
			System.exit(0);
		}

	}

	public void initialSearch(ArrayList<Film> filmsearch , boolean desort , String sortNumber) {

	
		switch (sortNumber) {
		case "Name":
			Collections.sort(filmsearch, new SortByName(desort));
			break;
		case "Year":
			Collections.sort(filmsearch, new SortByYear(desort));
			break;
		case "Duration":
			Collections.sort(filmsearch, new SortByDuration(desort));
			break;
		case "Director":
			Collections.sort(filmsearch, new SortByDirector(desort));
			break;
		case "Country":
			Collections.sort(filmsearch, new SortByCountry(desort));
			break;
		default :
			System.exit(0);
			break;
	
			
		}

		

	}

}
