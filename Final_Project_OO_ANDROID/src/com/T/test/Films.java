package com.T.test;

import java.util.Arrays;

public class Films {

	public String name;
	public int year;
	private String country;
	private String[] genreList;
	private int durationMinutes;
	private String director;
	private String description;

	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < genreList.length; i++)
			if (i == 0)
				s = genreList[i];
			else
				s = s + "  , " + genreList[i];

		return "Name: " + name + " \n Year: " + year + "\n  Country  "
				+ country + "\n    genreList:(" + s
				+ "  )  \n      durationMinutes: " + durationMinutes
				+ "\n       director: " + director + "\n        description: "
				+ description;
	}

	public boolean isEqual(String isequall) {
		if (this.name.contains(isequall)
				|| isequall.equals(Integer.toString(year))
				|| isequall.equals(Integer.toString(durationMinutes))
				|| this.director.contains(isequall)
				|| this.description.contains(isequall)
				|| this.country.equals(isequall)
				|| Arrays.binarySearch(this.genreList, isequall) > -1)

			return true;
		return false;
	}

}
