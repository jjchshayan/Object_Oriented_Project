package com.T.test;

import java.util.Comparator;

class SortByName implements Comparator<Film> {

	private boolean deSort;

	public SortByName(boolean deSort) {
		this.deSort = deSort;
	}

	@Override
	public int compare(Film x, Film y) {
		if (deSort == true)
			return y.getName().compareToIgnoreCase(x.getName());
		else
			return x.getName().compareToIgnoreCase(y.getName());
	}
}

class SortByYear implements Comparator<Film> {
	private boolean deSort;

	public SortByYear(boolean deSort) {
		this.deSort = deSort;
		;
	}

	@Override
	public int compare(Film x, Film y) {
		if (deSort == true)
			return y.getYear() - x.getYear();
		return x.getYear() - y.getYear();
	}
}

class SortByDuration implements Comparator<Film> {
	private boolean deSort;

	public SortByDuration(boolean deSort) {
		this.deSort = deSort;
	}

	@Override
	public int compare(Film x, Film y) {
		if (deSort == true)
			return y.getDurationMinutes() - x.getDurationMinutes();
		return x.getDurationMinutes() - y.getDurationMinutes();
	}
}

class SortByDirector implements Comparator<Film> {
	private boolean deSort;

	public SortByDirector(boolean deSort) {
		this.deSort = deSort;
	}

	@Override
	public int compare(Film x, Film y) {
		if (deSort == true)
			return y.getDirector().compareToIgnoreCase(x.getDirector());
		return x.getDirector().compareToIgnoreCase(y.getDirector());
	}
}

class SortByCountry implements Comparator<Film> {
	private boolean deSort;

	public SortByCountry(boolean deSort) {
		this.deSort = deSort;
	}

	@Override
	public int compare(Film x, Film y) {
		if (deSort == true)
			return y.getCountry().compareToIgnoreCase(x.getCountry());
		return x.getCountry().compareToIgnoreCase(y.getCountry());
	}
}
