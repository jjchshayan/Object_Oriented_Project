package com.T.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import android.util.Log;
import android.widget.ArrayAdapter;
import DB.FilmRepository;

public class Manager {

	private Scanner in;
	protected ArrayList<Film> filmsearch;
	Controll control;

	public void initailState() {
		control = new Controll();

	}

	public void isertJsonToDB() {
		control.converJsonToText();

	}

	 public ArrayList<Film> getDataEventBetweenTimes(String startdate , String enddate) {
	

		return control.geteventbettweendates(startdate, enddate);

	}

	public void changepassword(String usernameloggin, String passwordlogin,
			String newusername, String newupassword) {
	}

	public void inputnewRefree(String name, String password) {

		control.insetNewRefree(name, password);

	}

	public void inputnewfilm(String name, int year, String country,
			int durationMinutes, String director, String description,
			String[] genrelist) {
		control.addFilm(new Film(name, year, country, durationMinutes,
				director, description, genrelist));
	}

	public ArrayList<Film> Search(String name, ArrayAdapter<String> adapter,
			String searchby) {

		filmsearch = FilmRepository.getRepository().find(name, searchby);
		
		
		return filmsearch;

	}

	protected void printSearch(ArrayList<Film> filmsearch) {
		String[] columnNames = { "Name", "Year", "Duration", "Director",
				"Country", "Genre", "Description" };

		Object[][] data = null;
		data = new Object[filmsearch.size()][7];
		int i = 0;
		for (Film f : filmsearch) {
			data[i][0] = f.getName();
			data[i][1] = f.getYear();
			data[i][2] = f.getDurationMinutes();
			data[i][3] = f.getDirector();
			data[i][4] = f.getCountry();
			for (String s : f.getGenreList()) {
				if (data[i][5] == null)
					data[i][5] = s + "  ";
				else
					data[i][5] += s + "  ";
			}

			data[i][6] = f.getDescription();
			i++;
		}

	}

	public void updatadata(int id , String oldname, int oldyear, String oldcountry,
			int oldduration, String olddirector, String[] oldgenre,
			String olddescrib, String name, int year, String country,
			int duration, String director, String[] genre, String describ) {
		control.updatefilm(id, oldname, oldyear, oldcountry, oldduration,
				olddirector, oldgenre, olddescrib, name, year, country,
				duration, director, genre, describ);

	}

	public void newUserCreate(String newusername, String newpassword) {
		
		
	}

	public void initialSearch(ArrayList<Film> s, boolean checked, String string) {
		// TODO Auto-generated method stub
		
	}

	

}
