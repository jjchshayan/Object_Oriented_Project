package DB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import android.os.Environment;

public class Config {
	
	
	protected Properties readConfigFile(){
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream(Environment.getExternalStorageDirectory()
					+ "/config.properties");
			prop.load(input);
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			if(input!=null)
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
			
		}
		return prop;
		
		
	}
	
	
	
	
	private void writeToConfigFile(){
		

		Properties prop = new Properties();
		OutputStream out = null;
		
		try {
			out = new FileOutputStream("config.properties");
			
			prop.setProperty("database", "localhost");
			prop.setProperty("user", "root");
			prop.setProperty("password", "");
			
			prop.store(out, null);
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(out !=null)
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
		}
		
		
	}

}
