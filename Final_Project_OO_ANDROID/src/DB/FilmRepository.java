package DB;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import android.annotation.SuppressLint;

import com.T.test.Film;
import com.T.test.Films;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.mysql.cj.fabric.xmlrpc.base.Array;

@SuppressLint("NewApi")
public class FilmRepository {
	public static Logger log = Logger.getLogger(FilmRepository.class);
	private static FilmRepository repository;

	public static FilmRepository getRepository() {
		if (repository == null) {
			repository = new FilmRepository();
			// PropertyConfigurator.configure("log4j.properties");
		}
		return repository;
	}

	public ArrayList<Film> find(String inputSearch, String searchby) {
		Statement stmt = null;
		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".film Where " + searchby + "='" + inputSearch + "'";

		// System.out.println(query + "   " + inputSearch);

		ArrayList<Film> filmSearch = new ArrayList<Film>();
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {

				setDataForShowResult(rs, filmSearch);

			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return filmSearch;

	}

	private void setDataForShowResult(ResultSet rs, ArrayList<Film> filmSearch) {
		try {
			String name = rs.getString("name");
			int year = rs.getInt("year");
			int duration = rs.getInt("duration");
			String director = rs.getString("director");
			String country = rs.getString("country");
			String description = rs.getString("description");

			int id = rs.getInt("id");
			String[] genre = getGenreResulltSearch(id);

			filmSearch.add(new Film(name, year, country, duration, director,
					description, genre));
			filmSearch.get(filmSearch.size() - 1).setId(id);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String[] getGenreResulltSearch(int id) {
		String querygenre = "select * " + " from " + DB.getDB().getDbName()
				+ ".film_has_genre Where film_id='" + id + "'";

		Statement stmt;
		String[] outgenre = null;
		// String[] outgenre ;
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(querygenre);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			outgenre = new String[rowcount];

			while (rs.next()) {
				// System.out.println(rs.getRow()-1);
				outgenre[rs.getRow() - 1] = rs.getString("genre_name");

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return outgenre;

	}

	public void insetNewUser(String userName, String password) {

		String insertNewUser = "INSERT INTO users"
				+ "(Username,Password) VALUES" + "(?,?)";
		// System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.setString(1, userName);
			stmts.setString(2, password);
			stmts.executeUpdate();
			log.debug("New user with username:" + userName + " is signup ");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void changeNewUserdata(String olduserName, String oldpassword,
			String newusername, String newupassword) {

		String insertNewUser = "UPDATE users " + "SET Username='" + newusername
				+ "', Password='" + newupassword + "' WHERE Username ='"
				+ olduserName + "'" + "AND Password=" + "'" + oldpassword + "'";
		System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.executeUpdate();
			// log.debug("Update user "+ olduserName +" to "+newusername);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updatedata(int id,String oldname, int oldyear, String oldcountry,
			int oldduration, String olddirector, String[] oldgenre,
			String olddescrib , String name, int year, String country,
			int duration, String director, String[] genre,
			String describ) {

		String insertNewUser = "UPDATE film " + "SET name='" + name
				+ "', year='" + year + "', country='" + country +
				"', duration='" + duration + "', director='" + director +
				"', description='" + describ +
				

				"' WHERE name='" + oldname
				+ "'AND year='" + oldyear + "'AND country='" + oldcountry +
				"'AND duration='" + oldduration + "'AND director='" + olddirector +
				"'AND description='" + olddescrib + "'";
		System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.executeUpdate();
			// log.debug("Update user "+ olduserName +" to "+newusername);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ResultSet uprs2 = null;
		Statement stmt2 = null;
		try {
			stmt2 = DB
					.getDB()
					.getCon()
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);
			
			uprs2 = stmt2.executeQuery("SELECT * FROM "
					+ DB.getDB().getDbName() + ".film_has_genre");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		for (String genres : genre) {
			try {
				insertGenreIfNotExist(genres);
				uprs2.moveToInsertRow();
				uprs2.updateInt("film_id", id);
				uprs2.updateString("genre_name", genres);
				uprs2.insertRow();
				uprs2.beforeFirst();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		if (stmt2 != null) {
			try {
				stmt2.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

	}

	public void insetNewRefree(String userName, String password) {

		String insertNewUser = "INSERT INTO referee"
				+ "(Username,password) VALUES" + "(?,?)";
		// System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.setString(1, userName);
			stmts.setString(2, password);
			stmts.executeUpdate();
			log.debug("New referee with username:" + userName + " is signup ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void addFilm(Film film) throws SQLException {
		// Statement stmt=null;
		Statement stmt2 = null;
		java.sql.PreparedStatement stmts = null;
		try {

			String insertTableSQL = "INSERT INTO film"
					+ "(name,country,year,duration,director,description) VALUES"
					+ "(?,?,?,?,?,?)";

			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertTableSQL,
							Statement.RETURN_GENERATED_KEYS);

			stmts.setString(1, film.getName());
			stmts.setString(2, film.getCountry());
			stmts.setInt(3, film.getYear());
			stmts.setInt(4, film.getDurationMinutes());
			stmts.setString(5, film.getDirector());
			stmts.setString(6, film.getDescription());
			stmts.executeUpdate();

			ResultSet keys = stmts.getGeneratedKeys();
			keys.next();
			int key = keys.getInt(1);
			film.setId(key);

			log.debug("add new film with manager with name = "
					+ film.getName().toString().replace("'", "^"));
			// film.setId(uprs.getInt("id"));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmts != null) {
				stmts.close();
			}
		}

		stmt2 = DB
				.getDB()
				.getCon()
				.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
		ResultSet uprs2 = stmt2.executeQuery("SELECT * FROM "
				+ DB.getDB().getDbName() + ".film_has_genre");

		for (String genre : film.getGenreList()) {
			try {
				insertGenreIfNotExist(genre);
				uprs2.moveToInsertRow();
				uprs2.updateInt("film_id", film.getId());
				uprs2.updateString("genre_name", genre);
				uprs2.insertRow();
				uprs2.beforeFirst();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		if (stmt2 != null) {
			stmt2.close();
		}
	}

	private void insertGenreIfNotExist(String genreName) {
		Statement stmt = null;
		String query = "select * " + "from " + DB.getDB().getDbName()
				+ ".genre where name='" + genreName + "'";
		System.out.println(query);

		try {
			stmt = DB
					.getDB()
					.getCon()
					.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							ResultSet.CONCUR_UPDATABLE);
			ResultSet rs = stmt.executeQuery(query);

			if (!rs.next()) {
				rs.moveToInsertRow();
				rs.updateString("name", genreName);
				rs.insertRow();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void readAndInsertFromJson(String fileName)
			throws JsonSyntaxException, SQLException {

		File file = new File(fileName);
		/*
		 * / try (BufferedReader reader = new BufferedReader(new
		 * FileReader(file))) {
		 * 
		 * Gson gson = new GsonBuilder().create(); String i;
		 * 
		 * while ((i = reader.readLine()) != null) {
		 * 
		 * addFilm(gson.fromJson(i, Film.class));
		 * 
		 * }
		 * 
		 * } catch (FileNotFoundException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }/
		 */
	}

	public boolean checkUserAndPasswordTrue(String username, String password,
			String tblname) {

		String query = "select * " + " from " + DB.getDB().getDbName() + "."
				+ tblname + " Where Username='" + username
				+ "' AND  Password='" + password + "'";

		Statement stmt = null;
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;

	}

	public boolean checkUserRefereeTrue(String username) {

		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".referee Where name='" + username + "'";

		Statement stmt = null;
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;

	}

	public void addComment(String comment, int id_film, String name_film) {

		String insertNewUser = "INSERT INTO comment_reffree"
				+ "(ID_referee1,ID_Film,film_name , comment,ID_referee2,ID_referee3) VALUES"
				+ "(?,?,?,?,?,?)";
		System.out.println(insertNewUser);
		List<String> repeatedValue = new ArrayList<>();
		String referee1 = getrandomreferee();
		String referee2 = getrandomreferee();
		String referee3 = getrandomreferee();
		repeatedValue.add(referee1);
		while (true) {
			if (!repeatedValue.contains(referee2)) {
				repeatedValue.add(referee2);
				break;
			} else
				continue;

		}
		while (true) {
			if (!repeatedValue.contains(referee3)) {
				repeatedValue.add(referee3);
				break;
			} else
				continue;

		}

		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.setString(1, repeatedValue.get(0));
			stmts.setInt(2, id_film);
			stmts.setString(3, name_film);
			stmts.setString(4, comment);
			stmts.setString(5, repeatedValue.get(1));
			stmts.setString(6, repeatedValue.get(2));
			stmts.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}
	}

	private String getrandomreferee() {

		String query = "SELECT * FROM referee ORDER BY RAND() LIMIT 1";

		Statement stmt = null;
		String name = null;
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {

				name = rs.getString("Username");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		return name;
	}

	@SuppressWarnings("finally")
	public ArrayList<String> searchcomment(String name, int id) {
		Statement stmt = null;

		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".comment Where Film_Name  ='" + name + "' AND ID_film='"
				+ id + "'";

		ArrayList<String> array = new ArrayList<>();

		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {
				array.add(rs.getString("Comment"));
				// System.out.println(rs.getString("Film_Name") +
				// ":  "+rs.getString("Comment"));

			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (final SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return array;
		}

	}

	public ArrayList<String> getallcomment(String username) {

		Statement stmt = null;
		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".comment_reffree Where " + "ID_referee1" + "='" + username
				+ "' OR ID_referee2 = '" + username + "' OR ID_referee3 = '"
				+ username + "'";

		// System.out.println(query + "   " + inputSearch);

		ArrayList<String> l = new ArrayList<String>();
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {

				l.add(rs.getString("Comment"));
				// System.out.println(comment + " ");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return l;

	}

	public int getidrefereecomment(String username, String comment) {

		Statement stmt = null;
		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".comment_reffree Where " + "ID_referee1" + "='" + username
				+ "' OR ID_referee2" + "='" + username + "' OR ID_referee3"
				+ "='" + username + "' AND comment='" + comment + "'";

		System.out.println(query + "   ");

		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {

				return rs.getInt("ID");

				// System.out.println(comment + " ");

			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return -1;

	}

	public void setcommentaccept(String username, String message,
			int Acceptordontaccept) {
		int selectedID = getidrefereecomment(username, message);

		Statement stmt = null;
		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".comment_reffree Where " + "ID" + "='" + selectedID + "'";

		// System.out.println(query + "   " + inputSearch);

		int NumberOfAccept = 0;
		int NumberOfAcceptordesaccept = 0;

		String filmname = null;
		String comment = null;
		String referee1 = null;
		String referee2 = null;
		String referee3 = null;
		int filmID = 0;
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {

				NumberOfAccept = rs.getInt("NumberOfAccept");
				NumberOfAcceptordesaccept = rs
						.getInt("numallacceptordesacsept");
				filmname = rs.getString("film_name");
				filmID = rs.getInt("ID_Film");
				comment = rs.getString("comment");
				referee1 = rs.getString("ID_referee1");
				referee2 = rs.getString("ID_referee2");
				referee3 = rs.getString("ID_referee3");
				System.out.println(NumberOfAccept + " ");

				if (Acceptordontaccept == 1) {
					NumberOfAccept++;
					NumberOfAcceptordesaccept++;
				} else if (Acceptordontaccept == 2) {

					NumberOfAcceptordesaccept++;

				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		String updaterefere = null;
		if (referee1.equals(username)) {
			updaterefere = "ID_referee1";
		} else if (referee2.equals(username)) {
			updaterefere = "ID_referee2";
		} else if (referee3.equals(username)) {
			updaterefere = "ID_referee2";
		}
		String insertNewUser = "UPDATE comment_reffree "
				+ "SET NumberOfAccept='" + NumberOfAccept + "'"
				+ ", numallacceptordesacsept='" + NumberOfAcceptordesaccept
				+ "', " + updaterefere + "='" + "" + "'" + " WHERE ID ='"
				+ selectedID + "'";
		System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
			if (NumberOfAccept > 2) {
				deleterow(selectedID);
				updatecommenttable(filmID, filmname, comment);
			} else {
				if (NumberOfAcceptordesaccept - NumberOfAccept >= 2 || NumberOfAcceptordesaccept>3){
					deleterow(selectedID);
				}
			}
		

	}

	private void deleterow(Integer id) {
		String insertNewUser = " DELETE FROM comment_reffree WHERE ID='" + id
				+ "'";
		System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void updatecommenttable(int filmID, String filmname, String comment) {

		String insertNewUser = "INSERT INTO comment"
				+ "(ID_film,Comment, Film_Name) VALUES" + "(?,?,?)";

		String namereferee = getrandomreferee();
		// System.out.println(insertNewUser);
		java.sql.PreparedStatement stmts;
		try {
			stmts = DB
					.getDB()
					.getCon()
					.prepareStatement(insertNewUser,
							Statement.RETURN_GENERATED_KEYS);

			stmts.setInt(1, filmID);
			stmts.setString(2, comment);
			stmts.setString(3, filmname);

			stmts.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public ArrayList<Film> geteventbettweendates(String startdate, String enddate) {

		Statement stmt = null;
		String query = "select * " + " from " + DB.getDB().getDbName()
				+ ".film where Time between '" + startdate + "' AND '"
				+ enddate + "'";

		// System.out.println(query + "   " + inputSearch);
		ArrayList<Film> filmSearch = new ArrayList<>();
		try {
			stmt = DB.getDB().getCon().createStatement();
			ResultSet rs = stmt.executeQuery(query);

		
			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below
									// will move on, missing the first element
			}

			while (rs.next()) {

				setDataForShowResult2(rs, filmSearch);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return filmSearch;

	}
	

	private void setDataForShowResult2(ResultSet rs, ArrayList<Film> filmSearch) {
		try {
			String name = rs.getString("name");
			int year = rs.getInt("year");
			int duration = rs.getInt("duration");
			String director = rs.getString("director");
			String country = rs.getString("country");
			String description = rs.getString("description");
			String date = rs.getString("Time");
			int id = rs.getInt("id");
			String[] genre = getGenreResulltSearch(id);

			filmSearch.add(new Film(name, year, country, duration, director,
					description, genre,date));
			filmSearch.get(filmSearch.size() - 1).setId(id);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}