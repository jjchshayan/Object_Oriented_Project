package DB;

import java.sql.*;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.T.GUI.MainActivity;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.Formatter;
import android.util.Log;

public class DB {
	// public static Logger log = Logger.getLogger(DB.class);

	private String password = "1234";
	private String userName = "root";
	private String dbms = "mysql";
	private String serverName;
	private String portNumber = "3306";
	private String dbName = "film";

	private Connection con;
	private static DB db;

	public String getDbName() {
		return dbName;
	}

	public Connection getCon() {

		if (con != null)
			return con;
		else {

			return getConnection();
		}

	}

	public static DB getDB() {
		if (db == null) {

			db = new DB();
		}
		return db;
	}

	public Connection getConnection() {

		String s = Environment.getExternalStorageDirectory()
				+ "/log4j.properties";
		Log.d("ASDD", s);
		// PropertyConfigurator.configure(s);
		Connection conn = null;
		Config connectionProps = new Config();
		Properties prop = connectionProps.readConfigFile();

		serverName = "169.254.90.243";
		userName = prop.getProperty("user");
		password = prop.getProperty("password");
		dbName = prop.getProperty("dbname");

		// connectionProps.put("password", this.password);

		if (dbms.equals("mysql")) {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Log.d("ASDSADAD  ", "jdbc:mysql://" + serverName + ":3311/"
						+ dbName + "?user=" + userName + "&password="
						+ this.password);
				String url = "jdbc:mysql://" + serverName + ":3306/" + dbName
						+ "?useUnicode=true&characterEncoding=UTF-8";
				Log.d("ASDSADAD  ", url);
				// conn = DriverManager.getConnection("jdbc:mysql://" +
				// serverName
				// + ":3306/" + dbName + "?user=" + userName + "&password="
				// + password);
				conn = DriverManager.getConnection(url, userName+"s", password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Cannott connect to Database");
				System.exit(0);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("Connected to databases");
		// log.debug("Connected to database ");

		this.con = conn;

		return con;
	}

}
