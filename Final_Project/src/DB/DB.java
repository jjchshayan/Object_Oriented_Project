package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;


public class DB {
	private String password = "1234";
	private String userName = "root";
	private String dbms = "mysql";
	private String serverName;
	private String portNumber = "3306";
	private String dbName = "film";
	
	
	private Connection con;
	private Category log= Logger.getLogger(DB.class);;
	private static DB db;

	public String getDbName() {
		return dbName;
	}

	public Connection getCon() {
		return con;
	}

	public static DB getDB() {
		if (db == null) {
			db = new DB();
		}
		return db;
	}

	public Connection getConnection() {
	
		Connection conn = null;
		Config connectionProps = new Config();
		Properties prop = connectionProps.readConfigFile();

		serverName = prop.getProperty("servername");
		userName = prop.getProperty("user");
		password = prop.getProperty("password");
		dbName = prop.getProperty("dbname");

		// connectionProps.put("password", this.password);

		if (this.dbms.equals("mysql")) {
			try {
				String url = "jdbc:mysql://" + serverName
						+ ":3306/" + dbName+"?useUnicode=true&characterEncoding=UTF-8";
			
				
				
				conn = DriverManager.getConnection(url,userName,password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Cannott connect to Database");
				System.exit(0);
			}
		}
	
		System.out.println("Connected to databases");
		log.debug("Connected to database ");
		this.con = conn;
		return conn;
	}

	public String getPortNumber() {
		return portNumber;
	}

	public void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}

}
