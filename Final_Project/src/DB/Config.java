package DB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Config {
	
	
	protected Properties readConfigFile(){
		Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream("config.properties");
			prop.load(input);
			
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		finally{
			if(input!=null)
				try {
					input.close();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
			
			
			
		}
		return prop;
		
		
	}
	
	
	
	
	@SuppressWarnings("unused")
	private void writeToConfigFile(){
		

		Properties prop = new Properties();
		OutputStream out = null;
		
		try {
			out = new FileOutputStream("config.properties");
			
			prop.setProperty("database", "localhost");
			prop.setProperty("user", "root");
			prop.setProperty("password", "");
			
			prop.store(out, null);
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}finally{
			if(out !=null)
				try {
					out.close();
				} catch (IOException e) {
				
					e.printStackTrace();
				}
				
			
		}
		
		
	}

}
