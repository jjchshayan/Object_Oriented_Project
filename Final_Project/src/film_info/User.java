package film_info;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import DB.FilmRepository;

public class User extends Manager {
	Scanner in;
	private String usernameloggin;
	private String passwordlogin;
	public static Logger log = Logger.getLogger(Manager.class);
	private Scanner new_login;

	public void initailState() {
		control = new Controll();
		PropertyConfigurator.configure("log4j.properties");
		System.out.println("1) New User?");
		System.out.println("2) LogIn?");
		new_login = new Scanner(System.in);
		int log_new = new_login.nextInt();
		switch (log_new) {
		case 1:
			newUserCreate();
			break;
		case 2:
			checkPaswword();
			break;
		default:
			System.out.println("ERROR");
			System.exit(0);
			break;

		}
	}

	private void newUserCreate() {
		System.out.println("New User Setup:");
		in = new Scanner(System.in);
		System.out.println("Please insert Username and Password");
		System.out.print("UserName: ");
		String username = in.next();
		System.out.print("Password: ");
		String password = in.next();

		FilmRepository.getRepository().insetNewUser(username, password);
		System.out.println("You are create Account");

		initailState();

	}

	private void checkPaswword() {
		System.out.println("Login User: ");
		in = new Scanner(System.in);
		System.out.println("Please insert Username and Password");
		System.out.print("UserName: ");
		String username = in.next();
		System.out.print("Password: ");
		String password = in.next();

		if (FilmRepository.getRepository().checkUserAndPasswordTrue(username,
				password , "users") == true) {
			log.debug("Connected " + username + " user to databases ");
			usernameloggin = username;
			passwordlogin = password;
			System.out.println("You Are Loggin");
			System.out.println("1)Search?");
			System.out.println("2)Change username and password ?");

			in = new Scanner(System.in);
			int num = in.nextInt();

			if (num == 1) {
				Search();
				initialSearch(filmsearch);
				insertComment();
			} else if (num == 2) {
				in = new Scanner(System.in);
				System.out.print("User name:");
				String newusername = in.nextLine();
				System.out.print("Password: ");
				String newupassword = in.nextLine();

				control.changeNewUserdata(usernameloggin, passwordlogin,
						newusername, newupassword);

			} else {
				System.out.println("Error");
				System.exit(0);
			}

		} else {
			System.out.println("You Should create Account");
			System.exit(0);
		}

	}

	private void insertComment() {
		System.out.println("1) Insert comment");
		System.out.println("2) Search comment");
		System.out.println("3) Exit");

		in = new Scanner(System.in);
		int numbers = in.nextInt();
		if (numbers == 1) {
			System.out.println("Select one of the result:");
			int filmresult = in.nextInt();
			filmresult--;
			if (filmresult <= filmsearch.size()) {
				in = new Scanner(System.in);
				System.out.print("Comment:  ");
				String comment = in.nextLine();
				System.out.print(filmsearch.get(filmresult).getId() + "  "
						+ comment);

				control.addComment(comment, filmsearch.get(filmresult).getId(),
						filmsearch.get(filmresult).getName());

				log.debug("insert comment" + usernameloggin + "in "
						+ filmsearch.get(filmresult).getName() + " film");
			}
		} else if (numbers == 2) {
			in = new Scanner(System.in);
			System.out.println("Select one of the result:");
			int filmresult = in.nextInt();
			filmresult--;

			System.out.println("input name :");
			in = new Scanner(System.in);
			String name = in.nextLine();
			control.searchcomment(name, filmsearch.get(filmresult).getId());
		} else if (numbers == 3) {
			System.exit(0);
		} else {
			System.out.println("Error");
			System.exit(0);
		}
		System.exit(0);
	}

	public void initialSearch(ArrayList<Film> filmsearch) {

		System.out.println("\n1)Sort By name \n" + "2)Sort By year \n"
				+ "3)Sort By Duration \n" + "4)Sort By Director  \n"
				+ "5)Sort By Country  \n" + "6)EXIT");
		in = new Scanner(System.in);
		int sortNumber = in.nextInt();

		System.out.println("1)Sort \n" + "2)DeSort  \n");
		in = new Scanner(System.in);
		int getdesort = in.nextInt();
		boolean desort;
		if (getdesort == 2)
			desort = false;
		else
			desort = true;
		switch (sortNumber) {
		case 1:
			if (desort == true)
				filmsearch.sort((Film o1, Film o2) -> o1.getName()
						.compareTo(o1.getName()));
			else
				filmsearch.sort((Film o1, Film o2) -> o2.getName()
						.compareTo(o1.getName()));
			break;
		case 2:
			if (desort == true)
				filmsearch.sort((Film o1, Film o2) -> o1.getYear()
						- o2.getYear());
			else
				filmsearch.sort((Film o1, Film o2) -> o2.getYear()
						- o1.getYear());
			break;
		case 3:
			if (desort == true)
			filmsearch.sort((Film o1, Film o2) -> o1.getDurationMinutes()
					- o2.getDurationMinutes());
			else
				filmsearch.sort((Film o1, Film o2) -> o2.getDurationMinutes()
						- o1.getDurationMinutes());
			break;
		case 4:
			if (desort == true)
				filmsearch.sort((Film o1, Film o2) -> o1.getDirector()
						.compareTo(o2.getDirector()));
			else
				filmsearch.sort((Film o1, Film o2) -> o2.getDirector()
						.compareTo(o1.getDirector()));
			break;
		case 5:
			if (desort == true)
				filmsearch.sort((Film o1, Film o2) -> o1.getCountry()
						.compareTo(o2.getCountry()));
			else
				filmsearch.sort((Film o1, Film o2) -> o2.getCountry()
						.compareTo(o1.getCountry()));
			break;
		case 6:
			System.exit(0);
			break;
		default:
			System.out.println("ERROR");
			System.exit(0);
			break;
		}

		printSearch(filmsearch);

	}

}
