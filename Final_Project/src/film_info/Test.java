package film_info;

import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import DB.DB;

public class Test {
	private static Manager user;
	private static Guest guest;
	public static Logger log = Logger.getLogger(DB.class);
	private static Scanner in;
	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		DB.getDB().getConnection();
		
		

		System.out.println("Please select one of them? " + "\n"
				+ "1)Manager"
				+ "\n"
				+ "2)User"
				+ "\n"
				+ "3)Guest"
				+ "\n"
				+ "4)Referee");
		in = new Scanner(System.in);
		final int input = in.nextInt();

		switch (input) {
		case 1:
			user =  new Manager();
			user.initailState();
			break;
		
		case 2:
			user = new User();
			user.initailState();
			break;

		case 3:
			user = new Guest();
			user.initailState();
			break;
		case 4:
			Referee ref= new Referee();
			ref.initailState();
			break;
		default:
			System.out.println("ERROR");
			System.exit(0);
			break;
		}

	}
	public static Guest getGuest() {
		return guest;
	}
	public static void setGuest(Guest guest) {
		Test.guest = guest;
	}

}
