package film_info;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import DB.FilmRepository;

public class Guest extends User{

	
	private Scanner search;

	@Override
	public void initailState() {
	
		Search();
	}
	
	
	
	@Override
	public void Search() {
		
		System.out.println("Please insert film name:");
		search = new Scanner(System.in);
		String name =search.nextLine();
		
		ArrayList<Film> filmSearchResult =FilmRepository.getRepository().find(name,"name");
		printSearch(filmSearchResult);
		if(filmSearchResult.size()==0){
			System.out.println("There are not any result");
			System.exit(0);
		}
		else
	    initialSearch(filmSearchResult);
		
	}
	
	@Override
	public void initialSearch(ArrayList<Film> filmSearchResult){
		
		System.out.println("\n1)Sort By name \n"
		         + "2)Desort By name \n"
		         + "3)EXIT");
		in= new Scanner(System.in);
		int getdesort = in.nextInt();
		switch(getdesort){
	case 1:	
		Collections.sort(filmSearchResult,new SortByName(false));
		break;
	case 2:
		Collections.sort(filmSearchResult,new SortByYear(true));
		break;
	case 3:
		System.exit(0);
		break;
	default:System.out.println("ERROR");
	        System.exit(0);
	        break;
		}
	}
	
	
}
