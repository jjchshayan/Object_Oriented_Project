package film_info;

import java.util.ArrayList;
import java.util.Scanner;

import DB.FilmRepository;
import dnl.utils.text.table.TextTable;

public class Manager {

	private Scanner in;
	protected ArrayList<Film> filmsearch;
    Controll control ;
	private int i;


	public void initailState() {
		control = new Controll();
		System.out.println("1) Search film ");
		System.out.println("2) Input new film ");
		System.out.println("3) Input new Refree ");
		System.out.println("4) Get Result between dates ");
		System.out.println("5) Insert json file to database ");

		in = new Scanner(System.in);
		int log_new = in.nextInt();
		switch (log_new) {
		case 1:
			Search();
			break;
		case 2:
			inputnewfilm();
			break;
		case 3:
			inputnewRefree();
			break;
		case 4:
			getDataEventBetweenTimes();
			break;
		case 5:
			isertJsonToDB();
			break;
		default:
			System.out.println("ERROR");
			System.exit(0);
			break;

		}

	}

	private void isertJsonToDB() {
		control.converJsonToText();
		
		
	}

	private void getDataEventBetweenTimes() {
		in = new Scanner(System.in);
		System.out.println("Please insert data (examle: 2016-07-18 20:50:22 ");
		System.out.print("Start Time : ");
		String  startdate = in.nextLine();
		System.out.print("End Time : ");
		String  enddate = in.nextLine();
		
		control.geteventbettweendates(startdate , enddate);
		
	}

	private void inputnewRefree() {
		in = new Scanner(System.in);
		System.out.println("name");
		String name = in.next();

		control.insetNewRefree(name);

	}

	private void inputnewfilm() {
		in = new Scanner(System.in);

		System.out.println("Name: ");
		String name = in.next();

		System.out.println("Year:");
		int year = in.nextInt();

		System.out.println("Country:");
		String country = in.next();

		System.out.println("Duration:");
		int durationMinutes = in.nextInt();

		System.out.println("Director:");
		String director = in.next();

		System.out.println("Description:");
		String description = in.next();
		ArrayList<String> arraygenre = new ArrayList<>();

		while (true) {

			System.out.println("1)set genre");
			System.out.println("2)exit");
			in = new Scanner(System.in);
			int number = in.nextInt();

			if (number == 1) {
				System.out.println("Genre:");
				String Genre = in.next();
				arraygenre.add(Genre);
			} else if (number == 2) {
				break;
			} else {
				System.out.println("Error");
				System.exit(0);

			}

		}

		String[] genrelist = new String[arraygenre.size()];
		genrelist = arraygenre.toArray(genrelist);

		control.addFilm(new Film(name, year, country, durationMinutes,
				director, description, genrelist));
	}

	public void Search() {
		in = new Scanner(System.in);
		System.out.println("1)Search By name");
		System.out.println("2)Search By year");
		System.out.println("3)Search By country");
		System.out.println("4)Search By director");
		System.out.println("5)Search By duration");

		int inputsearchby = in.nextInt();

		String searchby = null;
		switch (inputsearchby) {
		case 1:
			searchby = "name";
			break;
		case 2:
			searchby = "year";
			break;
		case 3:
			searchby = "country";
			break;
		case 4:
			searchby = "director";
			break;
		case 5:
			searchby = "duration";
			break;
		default:
			System.out.println("ERROR");
			System.exit(0);
			break;

		}
		Searching(searchby);
	}

	private void Searching(String searchby) {

		System.out.print(searchby + ":");
		in = new Scanner(System.in);
		String SearchParameter = in.nextLine();

		filmsearch = FilmRepository.getRepository().find(SearchParameter,
				searchby);
		printSearch(filmsearch);
		if (filmsearch.size() == 0) {
			System.out.println("There are not any result");
			System.exit(0);
		}

	}

	protected void printSearch(ArrayList<Film> filmsearch) {
		String[] columnNames = { "Name", "Year", "Duration", "Director",
				"Country", "Genre", "Description" };

		 Object[][]	data = new Object[filmsearch.size()][7];
		 i = 0;
		filmsearch.forEach(value -> setdatavalue(value,data));
		

		TextTable tt = new TextTable(columnNames, data);
		tt.setAddRowNumbering(true);
		tt.printTable();
	}

	private void setdatavalue(Film value, Object[][] data) {

		data[i][0] = value.getName();
		data[i][1] = value.getYear();
		data[i][2] = value.getDurationMinutes();
		data[i][3] = value.getDirector();
		data[i][4] = value.getCountry();
		for (String s : value.getGenreList()) {
			if (data[i][5] == null)
				data[i][5] = s + "  ";
			else
				data[i][5] += s + "  ";
		}

		data[i][6] = value.getDescription();
		i++;
	
		
	}

}
