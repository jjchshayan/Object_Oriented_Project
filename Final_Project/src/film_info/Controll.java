package film_info;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import DB.FilmRepository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class Controll {

	private ArrayList<Film> films;
	FilmRepository fr;

	public Controll() {
		fr = FilmRepository.getRepository();
	}

	public void converJsonToText() {
		fr = FilmRepository.getRepository();
		setFilms(new ArrayList<Film>());
		File file = new File("movies.json");
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			Gson gson = new GsonBuilder().create();
			String i;
			while ((i = reader.readLine()) != null) {
				i = reader.readLine();

				try {
					fr.addFilm(gson.fromJson(i, Film.class));
				} catch (JsonSyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void geteventbettweendates(String startdate, String enddate) {
		fr.geteventbettweendates(startdate, enddate);
	}

	public void insetNewRefree(String name) {
		fr.insetNewRefree(name);
		
	}

	public void addFilm(Film film) {
		try {
			fr.addFilm(film);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void changeNewUserdata(String usernameloggin, String passwordlogin,
			String newusername, String newupassword) {
		fr.changeNewUserdata(usernameloggin, passwordlogin, newusername, newupassword);
		
	}

	public void addComment(String comment, int id, String name) {
		fr.addComment(comment, id, name);
		
	}

	public void searchcomment(String name, int id) {
		fr.searchcomment(name, id);
		
	}

	public ArrayList<Film> getFilms() {
		return films;
	}

	public void setFilms(ArrayList<Film> films) {
		this.films = films;
	}

}
